
import React from 'react'
import Search from './Component/Search'

export default class App extends React.Component {
  render() {
    return (
      <Search/>
    )
  }
}
//Flexbox pour gérer les différentes tailles d'écran
//pour l'appliquer il faut ajouter aux style={{flex:1 =ca veut dire prendre la méme place que les autres composant et 2 2 fois plus ...etc}}
// pour le type d'alignement on applique aux style flexDirection:"row" pour horizontal
//on peut jouer sur comment les components s'alignent: on ajoute aux style
 // justifyContent:'center' pour centrer
 //alignItem:'center' pour aligner sur l'axe x plus d'info sur le site de react native
